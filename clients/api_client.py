import logging
import os

import allure
import pytest
import requests
import curlify
from dotenv import load_dotenv

load_dotenv()


class ApiClient:
    def __init__(self):
        self.base_url = os.getenv("BASE_URL")
        self.api_key = os.getenv("API_KEY")
        self.session = requests.session()
        self.session.headers = {}
        self.session.headers.update({"X-RapidAPI-Key": self.api_key})

    def post(self, endpoint,  data=None, headers=None) -> requests.Response:
        url: str = self.base_url + endpoint
        response: requests.Response = self.session.post(url, data=data, headers=headers)
        logging.info(curlify.to_curl(response.request))
        allure.attach(response, name="Response", attachment_type=allure.attachment_type.TEXT)
        return response

    def get(self, endpoint: str,  params=None, headers=None) -> requests.Response:
        url: str = self.base_url + endpoint
        response: requests.Response = self.session.get(url, params=params, headers=headers)
        logging.info(curlify.to_curl(response.request))
        allure.attach(response, name="Response", attachment_type=allure.attachment_type.TEXT)
        return response

    def delete(self, endpoint, headers=None) -> requests.Response:
        url: str = self.base_url + endpoint
        response: requests.Response = self.session.delete(url, headers=headers)
        logging.info(curlify.to_curl(response.request))
        allure.attach(response, name="Response", attachment_type=allure.attachment_type.TEXT)
        return response


@pytest.fixture(scope="session")
def api_client() -> ApiClient:
    return ApiClient()

