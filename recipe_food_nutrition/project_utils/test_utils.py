from PIL import Image
import allure
from pixelmatch.contrib.PIL import pixelmatch


def looks_alike(actual_path, reference_path):
    reference_image = Image.open(reference_path)
    downloaded_image = Image.open(actual_path)

    # Сравнение изображений и создание разности
    image_diff = Image.new("RGBA", reference_image.size)
    mismatch = pixelmatch(reference_image, downloaded_image, image_diff, includeAA=True)

    # Если есть несоответствия, прикрепляем изображения к отчету и возвращаем False
    if mismatch:
        allure.attach(reference_image.tobytes(), name="Reference", attachment_type=allure.attachment_type.PNG)
        allure.attach(downloaded_image.tobytes(), name="Actual", attachment_type=allure.attachment_type.PNG)
        allure.attach(image_diff.tobytes(), name="Mismatch", attachment_type=allure.attachment_type.PNG)
        return False

    return True


def save_image(response, path):
    with open(path, 'wb') as f:
        f.write(response.content)





