import os
import pytest
import requests
from dotenv import load_dotenv
import pytest
import requests
from jsonschema import validate
from jsonschema.exceptions import ValidationError


load_dotenv()


@pytest.fixture(scope="module")
def api_headers():
    return {
        "X-RapidAPI-Key": os.environ.get("API_KEY"),
        "Content-Type": "application/json"
    }



json_schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "array",
        "items": [
            {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer"
                    },
                    "title": {
                        "type": "string"
                    },
                    "image": {
                        "type": "string"
                    },
                    "imageType": {
                        "type": "string"
                    },
                    "calories": {
                        "type": "integer"
                    },
                    "protein": {
                        "type": "string"
                    },
                    "fat": {
                        "type": "string"
                    },
                    "carbs": {
                        "type": "string"
                    },
                    "alcohol": {
                        "type": "string"
                    },
                    "caffeine": {
                        "type": "string"
                    },
                    "copper": {
                        "type": "string"
                    },
                    "calcium": {
                        "type": "string"
                    },
                    "cholesterol": {
                        "type": "string"
                    },
                    "saturatedFat": {
                        "type": "string"
                    },
                    "vitaminA": {
                        "type": "string"
                    },
                    "vitaminC": {
                        "type": "string"
                    },
                    "vitaminD": {
                        "type": "string"
                    },
                    "vitaminE": {
                        "type": "string"
                    },
                    "vitaminK": {
                        "type": "string"
                    },
                    "vitaminB1": {
                        "type": "string"
                    },
                    "vitaminB2": {
                        "type": "string"
                    },
                    "vitaminB3": {
                        "type": "string"
                    },
                    "vitaminB5": {
                        "type": "string"
                    },
                    "vitaminB6": {
                        "type": "string"
                    },
                    "vitaminB12": {
                        "type": "string"
                    },
                    "fiber": {
                        "type": "string"
                    },
                    "folate": {
                        "type": "string"
                    },
                    "iron": {
                        "type": "string"
                    },
                    "magnesium": {
                        "type": "string"
                    },
                    "manganese": {
                        "type": "string"
                    },
                    "phosphorus": {
                        "type": "string"
                    },
                    "potassium": {
                        "type": "string"
                    },
                    "selenium": {
                        "type": "string"
                    },
                    "sodium": {
                        "type": "string"
                    },
                    "sugar": {
                        "type": "string"
                    },
                    "zinc": {
                        "type": "string"
                    }
                },
                "required": [
                    "id",
                    "title",
                    "image",
                    "imageType",
                    "calories",
                    "protein",
                    "fat",
                    "carbs",
                    "alcohol",
                    "caffeine",
                    "copper",
                    "calcium",
                    "cholesterol",
                    "saturatedFat",
                    "vitaminA",
                    "vitaminC",
                    "vitaminD",
                    "vitaminE",
                    "vitaminK",
                    "vitaminB1",
                    "vitaminB2",
                    "vitaminB3",
                    "vitaminB5",
                    "vitaminB6",
                    "vitaminB12",
                    "fiber",
                    "folate",
                    "iron",
                    "magnesium",
                    "manganese",
                    "phosphorus",
                    "potassium",
                    "selenium",
                    "sodium",
                    "sugar",
                    "zinc"
                ]
            },
            {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer"
                    },
                    "title": {
                        "type": "string"
                    },
                    "image": {
                        "type": "string"
                    },
                    "imageType": {
                        "type": "string"
                    },
                    "calories": {
                        "type": "integer"
                    },
                    "protein": {
                        "type": "string"
                    },
                    "fat": {
                        "type": "string"
                    },
                    "carbs": {
                        "type": "string"
                    },
                    "alcohol": {
                        "type": "string"
                    },
                    "caffeine": {
                        "type": "string"
                    },
                    "copper": {
                        "type": "string"
                    },
                    "calcium": {
                        "type": "string"
                    },
                    "cholesterol": {
                        "type": "string"
                    },
                    "saturatedFat": {
                        "type": "string"
                    },
                    "vitaminA": {
                        "type": "string"
                    },
                    "vitaminC": {
                        "type": "string"
                    },
                    "vitaminD": {
                        "type": "string"
                    },
                    "vitaminE": {
                        "type": "string"
                    },
                    "vitaminK": {
                        "type": "string"
                    },
                    "vitaminB1": {
                        "type": "string"
                    },
                    "vitaminB2": {
                        "type": "string"
                    },
                    "vitaminB3": {
                        "type": "string"
                    },
                    "vitaminB5": {
                        "type": "string"
                    },
                    "vitaminB6": {
                        "type": "string"
                    },
                    "vitaminB12": {
                        "type": "string"
                    },
                    "fiber": {
                        "type": "string"
                    },
                    "folate": {
                        "type": "string"
                    },
                    "iron": {
                        "type": "string"
                    },
                    "magnesium": {
                        "type": "string"
                    },
                    "manganese": {
                        "type": "string"
                    },
                    "phosphorus": {
                        "type": "string"
                    },
                    "potassium": {
                        "type": "string"
                    },
                    "selenium": {
                        "type": "string"
                    },
                    "sodium": {
                        "type": "string"
                    },
                    "sugar": {
                        "type": "string"
                    },
                    "zinc": {
                        "type": "string"
                    }
                },
                "required": [
                    "id",
                    "title",
                    "image",
                    "imageType",
                    "calories",
                    "protein",
                    "fat",
                    "carbs",
                    "alcohol",
                    "caffeine",
                    "copper",
                    "calcium",
                    "cholesterol",
                    "saturatedFat",
                    "vitaminA",
                    "vitaminC",
                    "vitaminD",
                    "vitaminE",
                    "vitaminK",
                    "vitaminB1",
                    "vitaminB2",
                    "vitaminB3",
                    "vitaminB5",
                    "vitaminB6",
                    "vitaminB12",
                    "fiber",
                    "folate",
                    "iron",
                    "magnesium",
                    "manganese",
                    "phosphorus",
                    "potassium",
                    "selenium",
                    "sodium",
                    "sugar",
                    "zinc"
                ]
            },
            {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer"
                    },
                    "title": {
                        "type": "string"
                    },
                    "image": {
                        "type": "string"
                    },
                    "imageType": {
                        "type": "string"
                    },
                    "calories": {
                        "type": "integer"
                    },
                    "protein": {
                        "type": "string"
                    },
                    "fat": {
                        "type": "string"
                    },
                    "carbs": {
                        "type": "string"
                    },
                    "alcohol": {
                        "type": "string"
                    },
                    "caffeine": {
                        "type": "string"
                    },
                    "copper": {
                        "type": "string"
                    },
                    "calcium": {
                        "type": "string"
                    },
                    "cholesterol": {
                        "type": "string"
                    },
                    "saturatedFat": {
                        "type": "string"
                    },
                    "vitaminA": {
                        "type": "string"
                    },
                    "vitaminC": {
                        "type": "string"
                    },
                    "vitaminD": {
                        "type": "string"
                    },
                    "vitaminE": {
                        "type": "string"
                    },
                    "vitaminK": {
                        "type": "string"
                    },
                    "vitaminB1": {
                        "type": "string"
                    },
                    "vitaminB2": {
                        "type": "string"
                    },
                    "vitaminB3": {
                        "type": "string"
                    },
                    "vitaminB5": {
                        "type": "string"
                    },
                    "vitaminB6": {
                        "type": "string"
                    },
                    "vitaminB12": {
                        "type": "string"
                    },
                    "fiber": {
                        "type": "string"
                    },
                    "folate": {
                        "type": "string"
                    },
                    "iron": {
                        "type": "string"
                    },
                    "magnesium": {
                        "type": "string"
                    },
                    "manganese": {
                        "type": "string"
                    },
                    "phosphorus": {
                        "type": "string"
                    },
                    "potassium": {
                        "type": "string"
                    },
                    "selenium": {
                        "type": "string"
                    },
                    "sodium": {
                        "type": "string"
                    },
                    "sugar": {
                        "type": "string"
                    },
                    "zinc": {
                        "type": "string"
                    }
                },
                "required": [
                    "id",
                    "title",
                    "image",
                    "imageType",
                    "calories",
                    "protein",
                    "fat",
                    "carbs",
                    "alcohol",
                    "caffeine",
                    "copper",
                    "calcium",
                    "cholesterol",
                    "saturatedFat",
                    "vitaminA",
                    "vitaminC",
                    "vitaminD",
                    "vitaminE",
                    "vitaminK",
                    "vitaminB1",
                    "vitaminB2",
                    "vitaminB3",
                    "vitaminB5",
                    "vitaminB6",
                    "vitaminB12",
                    "fiber",
                    "folate",
                    "iron",
                    "magnesium",
                    "manganese",
                    "phosphorus",
                    "potassium",
                    "selenium",
                    "sodium",
                    "sugar",
                    "zinc"
                ]
            },
            {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer"
                    },
                    "title": {
                        "type": "string"
                    },
                    "image": {
                        "type": "string"
                    },
                    "imageType": {
                        "type": "string"
                    },
                    "calories": {
                        "type": "integer"
                    },
                    "protein": {
                        "type": "string"
                    },
                    "fat": {
                        "type": "string"
                    },
                    "carbs": {
                        "type": "string"
                    },
                    "alcohol": {
                        "type": "string"
                    },
                    "caffeine": {
                        "type": "string"
                    },
                    "copper": {
                        "type": "string"
                    },
                    "calcium": {
                        "type": "string"
                    },
                    "cholesterol": {
                        "type": "string"
                    },
                    "saturatedFat": {
                        "type": "string"
                    },
                    "vitaminA": {
                        "type": "string"
                    },
                    "vitaminC": {
                        "type": "string"
                    },
                    "vitaminD": {
                        "type": "string"
                    },
                    "vitaminE": {
                        "type": "string"
                    },
                    "vitaminK": {
                        "type": "string"
                    },
                    "vitaminB1": {
                        "type": "string"
                    },
                    "vitaminB2": {
                        "type": "string"
                    },
                    "vitaminB3": {
                        "type": "string"
                    },
                    "vitaminB5": {
                        "type": "string"
                    },
                    "vitaminB6": {
                        "type": "string"
                    },
                    "vitaminB12": {
                        "type": "string"
                    },
                    "fiber": {
                        "type": "string"
                    },
                    "folate": {
                        "type": "string"
                    },
                    "iron": {
                        "type": "string"
                    },
                    "magnesium": {
                        "type": "string"
                    },
                    "manganese": {
                        "type": "string"
                    },
                    "phosphorus": {
                        "type": "string"
                    },
                    "potassium": {
                        "type": "string"
                    },
                    "selenium": {
                        "type": "string"
                    },
                    "sodium": {
                        "type": "string"
                    },
                    "sugar": {
                        "type": "string"
                    },
                    "zinc": {
                        "type": "string"
                    }
                },
                "required": [
                    "id",
                    "title",
                    "image",
                    "imageType",
                    "calories",
                    "protein",
                    "fat",
                    "carbs",
                    "alcohol",
                    "caffeine",
                    "copper",
                    "calcium",
                    "cholesterol",
                    "saturatedFat",
                    "vitaminA",
                    "vitaminC",
                    "vitaminD",
                    "vitaminE",
                    "vitaminK",
                    "vitaminB1",
                    "vitaminB2",
                    "vitaminB3",
                    "vitaminB5",
                    "vitaminB6",
                    "vitaminB12",
                    "fiber",
                    "folate",
                    "iron",
                    "magnesium",
                    "manganese",
                    "phosphorus",
                    "potassium",
                    "selenium",
                    "sodium",
                    "sugar",
                    "zinc"
                ]
            },
            {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer"
                    },
                    "title": {
                        "type": "string"
                    },
                    "image": {
                        "type": "string"
                    },
                    "imageType": {
                        "type": "string"
                    },
                    "calories": {
                        "type": "integer"
                    },
                    "protein": {
                        "type": "string"
                    },
                    "fat": {
                        "type": "string"
                    },
                    "carbs": {
                        "type": "string"
                    },
                    "alcohol": {
                        "type": "string"
                    },
                    "caffeine": {
                        "type": "string"
                    },
                    "copper": {
                        "type": "string"
                    },
                    "calcium": {
                        "type": "string"
                    },
                    "cholesterol": {
                        "type": "string"
                    },
                    "saturatedFat": {
                        "type": "string"
                    },
                    "vitaminA": {
                        "type": "string"
                    },
                    "vitaminC": {
                        "type": "string"
                    },
                    "vitaminD": {
                        "type": "string"
                    },
                    "vitaminE": {
                        "type": "string"
                    },
                    "vitaminK": {
                        "type": "string"
                    },
                    "vitaminB1": {
                        "type": "string"
                    },
                    "vitaminB2": {
                        "type": "string"
                    },
                    "vitaminB3": {
                        "type": "string"
                    },
                    "vitaminB5": {
                        "type": "string"
                    },
                    "vitaminB6": {
                        "type": "string"
                    },
                    "vitaminB12": {
                        "type": "string"
                    },
                    "fiber": {
                        "type": "string"
                    },
                    "folate": {
                        "type": "string"
                    },
                    "iron": {
                        "type": "string"
                    },
                    "magnesium": {
                        "type": "string"
                    },
                    "manganese": {
                        "type": "string"
                    },
                    "phosphorus": {
                        "type": "string"
                    },
                    "potassium": {
                        "type": "string"
                    },
                    "selenium": {
                        "type": "string"
                    },
                    "sodium": {
                        "type": "string"
                    },
                    "sugar": {
                        "type": "string"
                    },
                    "zinc": {
                        "type": "string"
                    }
                },
                "required": [
                    "id",
                    "title",
                    "image",
                    "imageType",
                    "calories",
                    "protein",
                    "fat",
                    "carbs",
                    "alcohol",
                    "caffeine",
                    "copper",
                    "calcium",
                    "cholesterol",
                    "saturatedFat",
                    "vitaminA",
                    "vitaminC",
                    "vitaminD",
                    "vitaminE",
                    "vitaminK",
                    "vitaminB1",
                    "vitaminB2",
                    "vitaminB3",
                    "vitaminB5",
                    "vitaminB6",
                    "vitaminB12",
                    "fiber",
                    "folate",
                    "iron",
                    "magnesium",
                    "manganese",
                    "phosphorus",
                    "potassium",
                    "selenium",
                    "sodium",
                    "sugar",
                    "zinc"
                ]
            },
            {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer"
                    },
                    "title": {
                        "type": "string"
                    },
                    "image": {
                        "type": "string"
                    },
                    "imageType": {
                        "type": "string"
                    },
                    "calories": {
                        "type": "integer"
                    },
                    "protein": {
                        "type": "string"
                    },
                    "fat": {
                        "type": "string"
                    },
                    "carbs": {
                        "type": "string"
                    },
                    "alcohol": {
                        "type": "string"
                    },
                    "caffeine": {
                        "type": "string"
                    },
                    "copper": {
                        "type": "string"
                    },
                    "calcium": {
                        "type": "string"
                    },
                    "cholesterol": {
                        "type": "string"
                    },
                    "saturatedFat": {
                        "type": "string"
                    },
                    "vitaminA": {
                        "type": "string"
                    },
                    "vitaminC": {
                        "type": "string"
                    },
                    "vitaminD": {
                        "type": "string"
                    },
                    "vitaminE": {
                        "type": "string"
                    },
                    "vitaminK": {
                        "type": "string"
                    },
                    "vitaminB1": {
                        "type": "string"
                    },
                    "vitaminB2": {
                        "type": "string"
                    },
                    "vitaminB3": {
                        "type": "string"
                    },
                    "vitaminB5": {
                        "type": "string"
                    },
                    "vitaminB6": {
                        "type": "string"
                    },
                    "vitaminB12": {
                        "type": "string"
                    },
                    "fiber": {
                        "type": "string"
                    },
                    "folate": {
                        "type": "string"
                    },
                    "iron": {
                        "type": "string"
                    },
                    "magnesium": {
                        "type": "string"
                    },
                    "manganese": {
                        "type": "string"
                    },
                    "phosphorus": {
                        "type": "string"
                    },
                    "potassium": {
                        "type": "string"
                    },
                    "selenium": {
                        "type": "string"
                    },
                    "sodium": {
                        "type": "string"
                    },
                    "sugar": {
                        "type": "string"
                    },
                    "zinc": {
                        "type": "string"
                    }
                },
                "required": [
                    "id",
                    "title",
                    "image",
                    "imageType",
                    "calories",
                    "protein",
                    "fat",
                    "carbs",
                    "alcohol",
                    "caffeine",
                    "copper",
                    "calcium",
                    "cholesterol",
                    "saturatedFat",
                    "vitaminA",
                    "vitaminC",
                    "vitaminD",
                    "vitaminE",
                    "vitaminK",
                    "vitaminB1",
                    "vitaminB2",
                    "vitaminB3",
                    "vitaminB5",
                    "vitaminB6",
                    "vitaminB12",
                    "fiber",
                    "folate",
                    "iron",
                    "magnesium",
                    "manganese",
                    "phosphorus",
                    "potassium",
                    "selenium",
                    "sodium",
                    "sugar",
                    "zinc"
                ]
            },
            {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer"
                    },
                    "title": {
                        "type": "string"
                    },
                    "image": {
                        "type": "string"
                    },
                    "imageType": {
                        "type": "string"
                    },
                    "calories": {
                        "type": "integer"
                    },
                    "protein": {
                        "type": "string"
                    },
                    "fat": {
                        "type": "string"
                    },
                    "carbs": {
                        "type": "string"
                    },
                    "alcohol": {
                        "type": "string"
                    },
                    "caffeine": {
                        "type": "string"
                    },
                    "copper": {
                        "type": "string"
                    },
                    "calcium": {
                        "type": "string"
                    },
                    "cholesterol": {
                        "type": "string"
                    },
                    "saturatedFat": {
                        "type": "string"
                    },
                    "vitaminA": {
                        "type": "string"
                    },
                    "vitaminC": {
                        "type": "string"
                    },
                    "vitaminD": {
                        "type": "string"
                    },
                    "vitaminE": {
                        "type": "string"
                    },
                    "vitaminK": {
                        "type": "string"
                    },
                    "vitaminB1": {
                        "type": "string"
                    },
                    "vitaminB2": {
                        "type": "string"
                    },
                    "vitaminB3": {
                        "type": "string"
                    },
                    "vitaminB5": {
                        "type": "string"
                    },
                    "vitaminB6": {
                        "type": "string"
                    },
                    "vitaminB12": {
                        "type": "string"
                    },
                    "fiber": {
                        "type": "string"
                    },
                    "folate": {
                        "type": "string"
                    },
                    "iron": {
                        "type": "string"
                    },
                    "magnesium": {
                        "type": "string"
                    },
                    "manganese": {
                        "type": "string"
                    },
                    "phosphorus": {
                        "type": "string"
                    },
                    "potassium": {
                        "type": "string"
                    },
                    "selenium": {
                        "type": "string"
                    },
                    "sodium": {
                        "type": "string"
                    },
                    "sugar": {
                        "type": "string"
                    },
                    "zinc": {
                        "type": "string"
                    }
                },
                "required": [
                    "id",
                    "title",
                    "image",
                    "imageType",
                    "calories",
                    "protein",
                    "fat",
                    "carbs",
                    "alcohol",
                    "caffeine",
                    "copper",
                    "calcium",
                    "cholesterol",
                    "saturatedFat",
                    "vitaminA",
                    "vitaminC",
                    "vitaminD",
                    "vitaminE",
                    "vitaminK",
                    "vitaminB1",
                    "vitaminB2",
                    "vitaminB3",
                    "vitaminB5",
                    "vitaminB6",
                    "vitaminB12",
                    "fiber",
                    "folate",
                    "iron",
                    "magnesium",
                    "manganese",
                    "phosphorus",
                    "potassium",
                    "selenium",
                    "sodium",
                    "sugar",
                    "zinc"
                ]
            },
            {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer"
                    },
                    "title": {
                        "type": "string"
                    },
                    "image": {
                        "type": "string"
                    },
                    "imageType": {
                        "type": "string"
                    },
                    "calories": {
                        "type": "integer"
                    },
                    "protein": {
                        "type": "string"
                    },
                    "fat": {
                        "type": "string"
                    },
                    "carbs": {
                        "type": "string"
                    },
                    "alcohol": {
                        "type": "string"
                    },
                    "caffeine": {
                        "type": "string"
                    },
                    "copper": {
                        "type": "string"
                    },
                    "calcium": {
                        "type": "string"
                    },
                    "cholesterol": {
                        "type": "string"
                    },
                    "saturatedFat": {
                        "type": "string"
                    },
                    "vitaminA": {
                        "type": "string"
                    },
                    "vitaminC": {
                        "type": "string"
                    },
                    "vitaminD": {
                        "type": "string"
                    },
                    "vitaminE": {
                        "type": "string"
                    },
                    "vitaminK": {
                        "type": "string"
                    },
                    "vitaminB1": {
                        "type": "string"
                    },
                    "vitaminB2": {
                        "type": "string"
                    },
                    "vitaminB3": {
                        "type": "string"
                    },
                    "vitaminB5": {
                        "type": "string"
                    },
                    "vitaminB6": {
                        "type": "string"
                    },
                    "vitaminB12": {
                        "type": "string"
                    },
                    "fiber": {
                        "type": "string"
                    },
                    "folate": {
                        "type": "string"
                    },
                    "iron": {
                        "type": "string"
                    },
                    "magnesium": {
                        "type": "string"
                    },
                    "manganese": {
                        "type": "string"
                    },
                    "phosphorus": {
                        "type": "string"
                    },
                    "potassium": {
                        "type": "string"
                    },
                    "selenium": {
                        "type": "string"
                    },
                    "sodium": {
                        "type": "string"
                    },
                    "sugar": {
                        "type": "string"
                    },
                    "zinc": {
                        "type": "string"
                    }
                },
                "required": [
                    "id",
                    "title",
                    "image",
                    "imageType",
                    "calories",
                    "protein",
                    "fat",
                    "carbs",
                    "alcohol",
                    "caffeine",
                    "copper",
                    "calcium",
                    "cholesterol",
                    "saturatedFat",
                    "vitaminA",
                    "vitaminC",
                    "vitaminD",
                    "vitaminE",
                    "vitaminK",
                    "vitaminB1",
                    "vitaminB2",
                    "vitaminB3",
                    "vitaminB5",
                    "vitaminB6",
                    "vitaminB12",
                    "fiber",
                    "folate",
                    "iron",
                    "magnesium",
                    "manganese",
                    "phosphorus",
                    "potassium",
                    "selenium",
                    "sodium",
                    "sugar",
                    "zinc"
                ]
            },
            {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer"
                    },
                    "title": {
                        "type": "string"
                    },
                    "image": {
                        "type": "string"
                    },
                    "imageType": {
                        "type": "string"
                    },
                    "calories": {
                        "type": "integer"
                    },
                    "protein": {
                        "type": "string"
                    },
                    "fat": {
                        "type": "string"
                    },
                    "carbs": {
                        "type": "string"
                    },
                    "alcohol": {
                        "type": "string"
                    },
                    "caffeine": {
                        "type": "string"
                    },
                    "copper": {
                        "type": "string"
                    },
                    "calcium": {
                        "type": "string"
                    },
                    "cholesterol": {
                        "type": "string"
                    },
                    "saturatedFat": {
                        "type": "string"
                    },
                    "vitaminA": {
                        "type": "string"
                    },
                    "vitaminC": {
                        "type": "string"
                    },
                    "vitaminD": {
                        "type": "string"
                    },
                    "vitaminE": {
                        "type": "string"
                    },
                    "vitaminK": {
                        "type": "string"
                    },
                    "vitaminB1": {
                        "type": "string"
                    },
                    "vitaminB2": {
                        "type": "string"
                    },
                    "vitaminB3": {
                        "type": "string"
                    },
                    "vitaminB5": {
                        "type": "string"
                    },
                    "vitaminB6": {
                        "type": "string"
                    },
                    "vitaminB12": {
                        "type": "string"
                    },
                    "fiber": {
                        "type": "string"
                    },
                    "folate": {
                        "type": "string"
                    },
                    "iron": {
                        "type": "string"
                    },
                    "magnesium": {
                        "type": "string"
                    },
                    "manganese": {
                        "type": "string"
                    },
                    "phosphorus": {
                        "type": "string"
                    },
                    "potassium": {
                        "type": "string"
                    },
                    "selenium": {
                        "type": "string"
                    },
                    "sodium": {
                        "type": "string"
                    },
                    "sugar": {
                        "type": "string"
                    },
                    "zinc": {
                        "type": "string"
                    }
                },
                "required": [
                    "id",
                    "title",
                    "image",
                    "imageType",
                    "calories",
                    "protein",
                    "fat",
                    "carbs",
                    "alcohol",
                    "caffeine",
                    "copper",
                    "calcium",
                    "cholesterol",
                    "saturatedFat",
                    "vitaminA",
                    "vitaminC",
                    "vitaminD",
                    "vitaminE",
                    "vitaminK",
                    "vitaminB1",
                    "vitaminB2",
                    "vitaminB3",
                    "vitaminB5",
                    "vitaminB6",
                    "vitaminB12",
                    "fiber",
                    "folate",
                    "iron",
                    "magnesium",
                    "manganese",
                    "phosphorus",
                    "potassium",
                    "selenium",
                    "sodium",
                    "sugar",
                    "zinc"
                ]
            },
            {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer"
                    },
                    "title": {
                        "type": "string"
                    },
                    "image": {
                        "type": "string"
                    },
                    "imageType": {
                        "type": "string"
                    },
                    "calories": {
                        "type": "integer"
                    },
                    "protein": {
                        "type": "string"
                    },
                    "fat": {
                        "type": "string"
                    },
                    "carbs": {
                        "type": "string"
                    },
                    "alcohol": {
                        "type": "string"
                    },
                    "caffeine": {
                        "type": "string"
                    },
                    "copper": {
                        "type": "string"
                    },
                    "calcium": {
                        "type": "string"
                    },
                    "cholesterol": {
                        "type": "string"
                    },
                    "saturatedFat": {
                        "type": "string"
                    },
                    "vitaminA": {
                        "type": "string"
                    },
                    "vitaminC": {
                        "type": "string"
                    },
                    "vitaminD": {
                        "type": "string"
                    },
                    "vitaminE": {
                        "type": "string"
                    },
                    "vitaminK": {
                        "type": "string"
                    },
                    "vitaminB1": {
                        "type": "string"
                    },
                    "vitaminB2": {
                        "type": "string"
                    },
                    "vitaminB3": {
                        "type": "string"
                    },
                    "vitaminB5": {
                        "type": "string"
                    },
                    "vitaminB6": {
                        "type": "string"
                    },
                    "vitaminB12": {
                        "type": "string"
                    },
                    "fiber": {
                        "type": "string"
                    },
                    "folate": {
                        "type": "string"
                    },
                    "iron": {
                        "type": "string"
                    },
                    "magnesium": {
                        "type": "string"
                    },
                    "manganese": {
                        "type": "string"
                    },
                    "phosphorus": {
                        "type": "string"
                    },
                    "potassium": {
                        "type": "string"
                    },
                    "selenium": {
                        "type": "string"
                    },
                    "sodium": {
                        "type": "string"
                    },
                    "sugar": {
                        "type": "string"
                    },
                    "zinc": {
                        "type": "string"
                    }
                },
                "required": [
                    "id",
                    "title",
                    "image",
                    "imageType",
                    "calories",
                    "protein",
                    "fat",
                    "carbs",
                    "alcohol",
                    "caffeine",
                    "copper",
                    "calcium",
                    "cholesterol",
                    "saturatedFat",
                    "vitaminA",
                    "vitaminC",
                    "vitaminD",
                    "vitaminE",
                    "vitaminK",
                    "vitaminB1",
                    "vitaminB2",
                    "vitaminB3",
                    "vitaminB5",
                    "vitaminB6",
                    "vitaminB12",
                    "fiber",
                    "folate",
                    "iron",
                    "magnesium",
                    "manganese",
                    "phosphorus",
                    "potassium",
                    "selenium",
                    "sodium",
                    "sugar",
                    "zinc"
                ]
            }
        ]
    }

# Define your base query parameters
base_query_params = {
    "limitLicense": "false",
    "minProtein": "0",
    "minVitaminC": "0",
    "minSelenium": "0",
    "maxFluoride": "50",
    "maxVitaminB5": "50",
    "maxVitaminB3": "50",
    "maxIodine": "50",
    "minCarbs": "0",
    "maxCalories": "250",
    "minAlcohol": "0",
    "maxCopper": "50",
    "maxCholine": "50",
    "maxVitaminB6": "50",
    "minIron": "0",
    "maxManganese": "50",
    "minSodium": "0",
    "minSugar": "0",
    "maxFat": "20",
    "minCholine": "0",
    "maxVitaminC": "50",
    "maxVitaminB2": "50",
    "minVitaminB12": "0",
    "maxFolicAcid": "50",
    "minZinc": "0",
    "offset": "0",
    "maxProtein": "100",
    "minCalories": "0",
    "minCaffeine": "0",
    "minVitaminD": "0",
    "maxVitaminE": "50",
    "minVitaminB2": "0",
    "minFiber": "0",
    "minFolate": "0",
    "minManganese": "0",
    "maxPotassium": "50",
    "maxSugar": "50",
    "maxCaffeine": "50",
    "maxCholesterol": "50",
    "maxSaturatedFat": "50",
    "minVitaminB3": "0",
    "maxFiber": "50",
    "maxPhosphorus": "50",
    "minPotassium": "0",
    "maxSelenium": "50",
    "maxCarbs": "100",
    "minCalcium": "0",
    "minCholesterol": "0",
    "minFluoride": "0",
    "maxVitaminD": "50",
    "maxVitaminB12": "50",
    "minIodine": "0",
    "maxZinc": "50",
    "minSaturatedFat": "0",
    "minVitaminB1": "0",
    "maxFolate": "50",
    "minFolicAcid": "0",
    "maxMagnesium": "50",
    "minVitaminK": "0",
    "maxSodium": "50",
    "maxAlcohol": "50",
    "maxCalcium": "50",
    "maxVitaminA": "50",
    "maxVitaminK": "50",
    "minVitaminB5": "0",
    "maxIron": "50",
    "minCopper": "0",
    "maxVitaminB1": "50",
    "number": "10",
    "minVitaminA": "0",
    "minPhosphorus": "0",
    "minVitaminB6": "0",
    "minFat": "5",
    "minVitaminE": "0"}


@pytest.mark.parametrize("query_param", list(base_query_params.keys()))
def test_find_by_nutrients_with_query_params(api_headers, query_param):
    # Copy base query parameters and omit one
    query_params = base_query_params.copy()
    del query_params[query_param]

    response = requests.get(
        f"{os.getenv('BASE_URL')}/recipes/findByNutrients",
        headers=api_headers,
        params=query_params
    )

    assert response.status_code == 200, f"Failed with query parameter: {query_param}"

    # Validate JSON schema
    validate(instance=response.json(), schema=json_schema)


if __name__ == "__main__":
    pytest.main()
