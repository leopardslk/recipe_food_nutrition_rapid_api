import pytest


from clients.api_client import ApiClient


@pytest.fixture(scope="session")
def api_client() -> ApiClient:
    return ApiClient()


def test_get_request(api_client):
    endpoint = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/complexSearch"
    response = api_client.get(endpoint)
    assert response.status_code == 200

