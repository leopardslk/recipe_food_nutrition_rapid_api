import os

import allure
import pytest
from pytest_check import check
from dotenv import load_dotenv
import requests
from clients.api_client import ApiClient
from recipe_food_nutrition.project_utils.test_utils import looks_alike, save_image


@pytest.fixture(scope="session")
def api_client() -> ApiClient:
    return ApiClient()


@pytest.fixture(scope="module")
def api_headers() -> dict:
    return {
        "X-RapidAPI-Key": os.environ.get("API_KEY"),
        "Content-Type": "application/json"
    }


@pytest.fixture(scope="session")
def save_image(response, path):
    if os.path.exists("files/"):
        os.remove("files/")


base_url = os.getenv("BASE_URL")


@allure.feature("Product Images")
@allure.title("Test Product Images Comparison")
@pytest.mark.parametrize("product_id", ["1002531"])
def test_product_images_comparison(api_client: ApiClient, product_id: str) -> None:
    # Get product details
    response = api_client.get(f"{base_url}/food/products/{product_id}")
    assert response.status_code == 200, f'Could not get Product details {product_id}'

    # Check that 3 image urls were returned for product
    image_urls = response.json().get('images', [])
    assert len(image_urls) == 3, f'Expected: 3 image urls but found {len(image_urls)}'

    # Download images
    image_1_response = requests.get(image_urls[0])
    image_2_response = requests.get(image_urls[1])
    image_3_response = requests.get(image_urls[2])

    save_image(image_1_response, "files/image_1.png")
    save_image(image_2_response, "files/image_2.png")
    save_image(image_3_response, "files/image_3.png")

    with check:
        assert ("files/image_1.png", "files/image_ref_1.png")
    with check:
        assert looks_alike("files/image_2.png", "files/image_ref_2.png")
    with check:
        assert looks_alike("files/image_3.png", "files/image_ref_3.png")




























